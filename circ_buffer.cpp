template <typename T>
CircularBuffer<T>::CircularBuffer(const CircularBuffer<T>& other)
	: size(other.size)
{
	buffer = other.buffer;
	stored = other.stored;
}

template <typename T>
CircularBuffer<T>& CircularBuffer<T>::operator = (CircularBuffer<T>& other)
{
	if (this == &other)
	{
		return *this;
	}
	
	buffer = other.buffer;
	size = other.size;
	stored = other.stored;
	
	return *this;
}

template <typename T>
CircularBuffer<T>::CircularBuffer(CircularBuffer<T>&& other)
	: size(other.size)
{
	buffer = move(other.buffer);
	stored = other.stored;
	other.stored = 0;
}

template <typename T>
CircularBuffer<T>& CircularBuffer<T>::operator = (CircularBuffer<T>&& other)
{
	if (this == &other)
	{
		return *this;
	}
	
	buffer = move(other.buffer);
	size = other.size;
	stored = other.stored;
	other.stored = 0;
	
	return *this;
}

template <typename T>
void CircularBuffer<T>::push(const T& obj)
{
	lock_guard<mutex> lg(mtx);
	
	// If store limit is reached then do not allow to oveflow it and pop
	if (stored == size)
	{
		buffer.pop();
	}
	// If it is not reached then just increment stored
	else
	{
		stored++;
	}
	
	// In any case we already have free space for the new element
	buffer.push(obj);
}

template <typename T>
void CircularBuffer<T>::push_multiple(initializer_list<T> l)
{
	for (auto v : l)
	{
		push(v);
	}
}

template <typename T>
T CircularBuffer<T>::pop()
{
	lock_guard<mutex> lg(mtx);
	
	if (!stored)
	{
		throw runtime_error("Pop from empty queue");
	}
	
	auto res = buffer.front();
	buffer.pop();
	stored--;
	
	return res;
}

template <typename T>
void CircularBuffer<T>::clear()
{
	lock_guard<mutex> lg(mtx);
	stored = 0;
	buffer.clear();
}
