#include <random>
#include <future>
#include <algorithm>
#include <gtest/gtest.h>

#include "../circ_buffer.h"

using namespace std;

// This func is for single threaded environment only!
template<typename T>
bool test_buffer_with(CircularBuffer<T>& bf, const T* const arr, const size_t len)
{
	// Just go through buffer and check elements order.
	size_t i = len - bf.get_size();
	while (bf.get_elements_number() > 0)
	{
		if (bf.pop() != arr[i++])
		{
			return false;
		}
	}
	
	// Lets check that there are no more elements. Just for fun.
	return bf.get_elements_number() == 0;
}

TEST(CreationAndAssignmentTest, Positive) 
{
	// Future circular buffer size
	const size_t buffer_size = 5u;
	
	// Control array
	const size_t array_len = 7;
	int ctrl_arr[array_len] = {7,5,3,1,2,4,6};
	
	// Init from initializer list
	// Exactly same initializers with ctrl_arr
	CircularBuffer<int> bf1({7,5,3,1,2,4,6}, buffer_size);
	
	// Check if something was added to buffer
	EXPECT_EQ(bf1.get_elements_number(), buffer_size);
	
	// Test move constructor
	auto bf2(move(bf1));
	// Check if something was added to buffer
	EXPECT_EQ(bf2.get_elements_number(), buffer_size);
	
	
	// Copy operator
	auto bf3 = bf2;
	// Copy constructor
	auto bf4(bf2);
	// Just copying for future needs
	auto bf5(bf2);
	// Move operator
	auto bf6 = move(bf5);
	
	// Test all constructors and operators
	EXPECT_TRUE(test_buffer_with(bf2, ctrl_arr, array_len));
	EXPECT_TRUE(test_buffer_with(bf3, ctrl_arr, array_len));
	EXPECT_TRUE(test_buffer_with(bf4, ctrl_arr, array_len));
	EXPECT_TRUE(test_buffer_with(bf6, ctrl_arr, array_len));
}

TEST(PushPopAndLoadTest, Positive) 
{
	// Future buffer size
	const size_t buffer_1_size = 3u;
	// Control array
	vector<string> ctrl_1_arr = {"hello_world", "world_hello", "hi_world", 
								"world_world", "hello_hello", "hi_hello"};
								
	CircularBuffer<string> bf1(buffer_1_size);
	
	for (auto s : ctrl_1_arr)
	{
		bf1.push(s);
		EXPECT_EQ(s, bf1.pop());
	}

	// Future buffer size
	const size_t buffer_2_size = 7u;
	// Number of tests to perform
	const size_t test_number = 1000000u;
	
	// Init buffer			
	CircularBuffer<int> bf2(buffer_2_size);
	
	// Prepare distribution
	random_device rd;
    mt19937 gen(rd());
    uniform_int_distribution<> distrib(1, 100000);
    auto gen_fnc = std::bind(distrib, gen);
    
    // Perform push-pop tests
    long i = 0;
	while (i < test_number)
	{
		auto val = gen_fnc();
		bf2.push(val);
		EXPECT_EQ(val, bf2.pop());
		i++;
	}
}

TEST(MultithreadedTest, Positive)
{
	// Future buffer size
	const size_t buffer_size = 15u;
	
	// Number of points to generate
	const size_t test_number = 300000;
	
	// Initialize buffer
	CircularBuffer<int> bf(buffer_size);
	
	// Prepare distribution
	random_device rd;
    mt19937 gen(rd());
    uniform_int_distribution<> distrib(1, 100000);
    auto gen_fnc = std::bind(distrib, gen);
    
    // Fill control vectors
    vector<int> ctrl_vect_1;
    vector<int> ctrl_vect_2;
    for (int i = 0 ; i < test_number; ++i)
    {
		if (i < test_number / 2)
		{
			ctrl_vect_1.push_back(gen_fnc());
		}
		else
		{
			ctrl_vect_2.push_back(gen_fnc());
		}
	}
	
	// Start thread for first control vector
	future<void> ftr1(async([&ctrl_vect_1, &bf]()
		{
			for (auto v : ctrl_vect_1)
			{
				bf.push(v);
			}
		}));
	
	// Start thread for second control vector
	future<void> ftr2(async([&ctrl_vect_2, &bf]()
		{
			for (auto v : ctrl_vect_2)
			{
				bf.push(v);
			}
		}));
	
	// Wait for threads to finish
	ftr1.get();
	ftr2.get();
	
	// Check if we have all values from buffer in one of the control vectors
	while (bf.get_elements_number() > 0)
	{
		auto elem = bf.pop();
		EXPECT_TRUE(find(ctrl_vect_1.begin(), ctrl_vect_1.end(), elem) != ctrl_vect_1.end()
					|| find(ctrl_vect_2.begin(), ctrl_vect_2.end(), elem) != ctrl_vect_2.end());
	}
}

TEST(PopFromEmptyBuffer, Positive) 
{
	CircularBuffer<int> bf(5u);
	bool got_ex = false;
	try
	{
		bf.pop();
	}
	catch (runtime_error& e)
	{
		got_ex = true;
	}
	
	EXPECT_TRUE(got_ex);
}
