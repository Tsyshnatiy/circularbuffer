// Do not use pragma once because of related warning in this special case
#ifndef CIRC_BUFFER_H_
#define CIRC_BUFFER_H_

#include <cstdio>				// size_t
#include <queue>				// queue
#include <list>					// list for queue
#include <mutex>				// mutex / lock_guard
#include <initializer_list>		// initializer_list
#include <stdexcept>			// runtime_error

using namespace std;

// This class impls thread safe circular buffer based on queue(list)
template <typename T> 
class CircularBuffer 
{
protected:
	// Sync unit
	mutex mtx;
	
	// Queue based on list to avoid memory reallocation
	queue<T, list<T>> buffer;
	
	// Buffer size.
	const size_t size;
	
	// Currently stored elements number
	size_t stored = 0;
	
public:
	explicit CircularBuffer(const size_t size) 
		: size(size)
	{}
	
	CircularBuffer(initializer_list<T> l, const size_t size) 
		: CircularBuffer(size)
	{
		push_multiple(l);
	}
	
	CircularBuffer(const CircularBuffer& other);
	
	CircularBuffer& operator = (CircularBuffer& other);
	
	CircularBuffer(CircularBuffer&& other);
	
	CircularBuffer& operator = (CircularBuffer&& other);
	
	// Checks if buffer is empty
	bool empty() { return stored == 0; }
	
	// Clears buffer and context
	void clear();
	
	// Returns number of currently stored elements
	size_t get_elements_number() const { return stored; }
	
	// Returns total buffer size
	size_t get_size() const { return size; }
	
	// Pushes one element
	virtual void push(const T& obj);
	
	// Pushes several elements provided by initializer list
	virtual void push_multiple(initializer_list<T> l);
	
	// TODO Implement push_multiple for abstract Container if needed
	
	// Pops one element. 
	// Throws runtime_error if pop was called for empty buffer
	virtual T pop();
	
	virtual ~CircularBuffer() {}
};

#include "circ_buffer.cpp"

#endif // CIRC_BUFFER_H_
